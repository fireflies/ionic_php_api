<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');
	require("connection.php");
	$data = json_decode(file_get_contents("php://input"),true);
	$_POST = $data;
	if(isset($_POST['name'])){
		$name = $_POST['name'];
		try{
			$sql = "INSERT INTO files (name) VALUES (:names)";
			$statement = $conn->prepare($sql);
			$statement->bindValue(':names', $name);
			$statement->execute();
		}

		catch(PDOException $e) {
			echo "error:" . $e->getMessage();
		}
	}
?>