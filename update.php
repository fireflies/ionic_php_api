<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');
	require("connection.php");
	$data = json_decode(file_get_contents("php://input"),true);
	$_POST = $data;
	if(isset($_POST['Oldname']) && isset($_POST['newName'])){
		$Oname = $_POST['Oldname'];
		$Nname = $_POST['newName'];
		try{
			$sql = "SELECT * FROM files WHERE name = :names";
			$statement = $conn->prepare($sql);
			$statement->bindValue(':names', $Oname);
			$statement->execute();
			$returnValue = getdata($statement);
		}

		catch(PDOException $e) {
			echo "error:" . $e->getMessage();
		}


		try{
			$sql = "UPDATE files SET name = :Nname WHERE id = :id";
			$statement = $conn->prepare($sql);
			$statement->bindValue(':Nname', $Nname);
			$statement->bindValue(':id', $returnValue);
			$statement->execute();
		}

		catch(PDOException $e) {
			echo "error:" . $e->getMessage();
		}
	}

	function getdata($statement){
		$row = $statement->fetch(PDO::FETCH_ASSOC);
		return $row['id'];
	}

?>