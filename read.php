<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: GET');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');
	
	include("connection.php");
	

	try{
		$data = "SELECT * FROM files";
		$statement = $conn->prepare($data);
		$statement->execute();
		getdata($statement);
	}catch(PDOException $e){
		echo "error" . $e->getMessage();
	}

	function getdata($statement){
		$row = $statement->fetchAll(PDO::FETCH_ASSOC);
		echo json_encode($row);
	}
?>